﻿$(function () {
    assignChatBehaviour();
});

function assignChatBehaviour() {
    $(".chat-container").each(function () {
        this.scrollTop = this.scrollHeight;
    });

    $(".chat-send-message-form").submit(messageSubmitting);
}

function messageSent(data, status, xhr, formSelector) {
    if (!data || data.trim() == "") {
        return;
    }

    var $container = $(".chat-container");
    var $receivedHtml = $("<div/>").html(data);

    var count = $container.find(".chat-item").length;
    var directionCSS = count % 2 != 1 ? "" : "right";
    var timestampCSS = count % 2 != 1 ? "pull-right" : "pull-left";

    $receivedHtml.find(".chat-item").addClass(directionCSS);
    $receivedHtml.find(".chat-timestamp").addClass(timestampCSS);

    $container.append($receivedHtml.contents());
    $container.each(function () {
        this.scrollTop = this.scrollHeight;
    });

    $(formSelector).find(".chat-message").val('');
}

function messageSubmitting(event) {
    var $message = $(event.currentTarget).find(".chat-message").val();

    if ($message.trim() == "") {
        return false;
    }
}