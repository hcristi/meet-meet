﻿using Meet_Meet.WebApp.Models;
using Meet_Meet.WebApp.Repository;
using Microsoft.AspNet.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Meet_Meet.WebApp.SignalR
{
    public class SimpleMessagesHub : Hub
    {
        public async Task<ActionResult> GetMessage(string messageId)
        {
            var filter = Builders<MeetingMessageModel>.Filter.Eq(m => m.Id, messageId);
            MeetingMessageModel message = 
                await BaseRepository.GetMessagesCollection<MeetingMessageModel>().Find(filter).FirstOrDefaultAsync();

            return new EmptyResult();
        }
    }
}