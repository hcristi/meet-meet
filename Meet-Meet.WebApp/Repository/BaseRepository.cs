﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using MongoDB.Driver;

namespace Meet_Meet.WebApp.Repository
{
    public static class BaseRepository
    {
        private static MongoUrl mongoUrl = MongoUrl.Create(WebConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString);

        public static MongoClient MongoClient { get; private set; }

        public static IMongoDatabase MongoDatabase { get; private set; }

        static BaseRepository()
        {
            BaseRepository.MongoClient = new MongoClient(mongoUrl.ToString());
            BaseRepository.MongoDatabase = BaseRepository.MongoClient.GetDatabase(mongoUrl.DatabaseName);
        }

        /// <summary>
        /// Retrives the collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static IMongoCollection<T> RetriveCollection<T>(string collectionName)
        {
            return BaseRepository.MongoDatabase.GetCollection<T>(collectionName);
        }

        public static IMongoCollection<T> GetMessagesCollection<T>()
        {
            return BaseRepository.RetriveCollection<T>("messages");
        }
    }
}