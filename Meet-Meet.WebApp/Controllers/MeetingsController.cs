﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Meet_Meet.WebApp.Models;
using Meet_Meet.WebApp.Repository;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using MongoDB.Driver.GeoJsonObjectModel.Serializers;

namespace Meet_Meet.WebApp.Controllers
{
    public class MeetingsController : Controller
    {
        // GET: Meetings
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            IMongoCollection<MeetingMessageModel> collection = BaseRepository.GetMessagesCollection<MeetingMessageModel>();

            List<MeetingMessageModel> modelList = await collection.Find(new BsonDocument()).ToListAsync();
            
            return View(modelList);
        }

        /// <summary>
        /// Saves the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMessage(MessageModel messageModel)
        {
            if(!ModelState.IsValid)
            {
                return new EmptyResult();
            }

            IMongoCollection<MeetingMessageModel> collection = BaseRepository.GetMessagesCollection<MeetingMessageModel>();

            MeetingMessageModel meetingModel = new MeetingMessageModel()
            {
                Message = messageModel.Message,
                CreatedOn = DateTime.Now
            };

            await collection.InsertOneAsync(meetingModel);

            return PartialView("Partial/_MessagePartial", meetingModel);
        }
    }
}